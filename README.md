# QR web renderer

Primitive page that renders its URL search parameters.
Example:
```
?h=Report&l1=Daria+Bogatova&l2=Biology&s=warning

where:
	- h : heading and title
	- l1...l5 : lines
	- s : alert style in bootstrap (https://getbootstrap.com/docs/4.5/components/alerts/)
```
